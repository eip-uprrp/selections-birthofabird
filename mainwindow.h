#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "bird.h"
#include <QGraphicsScene>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /// \fn MainWindow::MainWindow(QWidget *parent)
    /// \~English
    /// \brief Constructor
    /// \~Spanish
    /// \brief Constructor
    explicit MainWindow(QWidget *parent = 0);

    /// \fn MainWindow::~MainWindow(QWidget *parent)
    /// \~English
    /// \brief Destructor
    /// \~Spanish
    /// \brief Destructor
    ~MainWindow();

    /// \fn void MainWindow::addBird(int x, int y, Bird &b)
    /// \~English
    /// \brief Paints a bird into the MainWindow
    /// \param x X coordinate of the Main Window
    /// \param y Y coordinate of the Main Window
    /// \param b Bird object to be painted
    /// \~Spanish
    /// \brief Pinta un pajaro en la pantalla principal
    /// \param x Coordenada X de la pantalla principal
    /// \param y Coordenada Y de la pantalla principal
    /// \param b Objecto Bird a ser pintado
    void addBird(int x, int y, Bird &b) ;


    /// \fn void MainWindow::addBird(Bird &b)
    /// \~English
    /// \brief Paints a bird into the MainWindow
    /// \param b Bird object to be painted
    /// \~Spanish
    /// \brief Pinta un pajaro en la pantalla principal
    /// \param b Objecto Bird a ser pintado
    void addBird(Bird &b) ;

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene ;
};

#endif // MAINWINDOW_H
