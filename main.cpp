#include <QApplication>
#include "mainwindow.h"
#include "bird.h"
#include <cstdlib>
#include <time.h>
#include <QTimer>
#include <QObject>
#include <QDebug>

void FilterBirds(Bird birds[], int N) ;

#include <QTime>
#include <QtMultimedia/QSound>

/// \file

/// \fn void birth(MainWindow &w, const Bird &mom, const Bird &dad, Bird &child)
/// \~English
/// \brief Function that given references to objects mom, dad and child, uses
/// the DNA of the father and the mother to generate DNA of the child.
/// This function is edited by the student.
/// \param w Reference to the main window
/// \param mom Reference to const object of type Bird.
/// \param dad Reference to const object of the Bird.
/// \param child Reference to object child that will be modified.
/// ~Spanish
/// \brief Funcion que dado referencias a los objetos mom, dad, y child,
/// usa el ADN de el padre (dad) y la madre (mom) para generar el ADN del nino (child).
/// Esta funcion es editada por el estudiante.
/// \param w Referencia a la pantalla principal
/// \param mom Referencia al objeto constante de tipo Bird.
/// \param dad Referencia al objeto constante de tipo Bird.
/// \param child Referencia al objeto de tipo Bird a ser modificado.
void birth(MainWindow &w, const Bird &mom, const Bird &dad, Bird &child)
{
    QSound::play( ":bird.wav" );

 
    child.setEyebrow(mom.getEyebrow());
    child.setFaceColor(dad.getFaceColor());
    
    //
    // YOUR CODE HERE
    // TU CODIGO VA AQUI
    //


    // ---- do not modify anything below this line ----
    // add the bird to the scene and show it
    // ---- no mofique nada bajo esta linea
    w.addBird(100, 210, child);
    w.show();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    Bird avelardo, juana, piolin ;

    // add juana and avelardo to the scene
    w.addBird(10,10,juana);
    w.addBird(210,10,avelardo);

    // create a timer and call the birth function at the trigger
    QTimer aTimer;
    aTimer.setInterval(1000);
    aTimer.setSingleShot(true);
    aTimer.start();
    QObject::connect(&aTimer,&QTimer::timeout, [&](){ birth(w, juana, avelardo, piolin);});


    w.show();
    return a.exec();
}
