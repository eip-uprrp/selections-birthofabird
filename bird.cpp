// 
// RAN [2014-06-02] 
//   - Changed the setEyebrow, setSize functions. Resize (+2,+2)
//   - EyeBrowType
// RAN [2014-06-03]
//   - TranslucentBackground for non-default constructor
//   - Some more comments

#include "bird.h"
#include <QDebug>

/// \fn Bird::Bird(QWidget *parent)
/// \~English
/// \brief Default constructor. The properties of the bird are set as follows:
/// size: set to random value of either 50, 100, 150 or 200
/// * eyeBrow: set randomly to one of the five possible values
/// * color and eColor: set randomly to one of the five possible colors:
/// "green", "blue", "red", "yellow", "white"
/// \~Spanish
/// \brief Constructor por defecto. Las propiedades de el pajaron son ajustados como sigue:
/// * eyeBrow: ajustado aleatoriamente a uno de los 5 posibles valores
/// * color y eColor: ajustao aleatoriamente a uno de los 5 posibles colores:
/// "verde", "azul", "rojo", "amarillo", "blanco"
///
Bird::Bird(QWidget *parent) :
    QWidget(parent)
{
    QString prism[5] = {"green", "blue", "red", "yellow", "white"} ;
    size = randInt(1, 4) * 50 ;
    resize(size+2,size+2) ;
    move(0,0) ;
    eyeBrow = (EyeBrowType)randInt(0,3);
    color = prism[randInt(0,4)] ;
    eColor = prism[randInt(0,4)] ;
    setAttribute(Qt::WA_TranslucentBackground);
    setStyleSheet("background:transparent;");
}

/// \fn Bird::Bird(int s, EyeBrowType eb, QString col, QString eye, QWidget *parent)
/// \~English
/// \brief Constructor which accepts specification of the attributes of the bird
/// \param s size
/// \param eb eyeBrow
/// \param col (faceColor)
/// \param eye (eyeColor)
/// \~Spanish
/// \brief Constructor que acepta especificaciones de los atributos del pajaro
/// \param s tamaño
/// \param eb cejas
/// \param col color de la cara
/// \param eye color del ojo
Bird::Bird(int s, EyeBrowType eb, QString col, QString eye, QWidget *parent):
    QWidget(parent)
{
    resize(s+2,s+2) ;
    move(0,0) ;
    size = s; eyeBrow = eb; color=col ; eColor = eye ;
    setAttribute(Qt::WA_TranslucentBackground);
    setStyleSheet("background:transparent;");
}

/// \fn QString  Bird::getEyeColor()
/// \~English
/// \brief Getter for the eyeColor.
/// \~Spanish
/// \brief Devuelve el color del ojo.
QString  Bird::getEyeColor() const {
    return eColor ;
}

/// \fn QString  Bird::getFaceColor()
/// \~English
/// \brief Getter for the faceColor.
/// \~Spanish
/// \brief Devuelve el color de la cara.
QString  Bird::getFaceColor() const {
    return color ;
}

/// \fn void  Bird::setEyeColor(QString eye)
/// \~English
/// \brief Setter for the eyeColor.
/// \param eye eyeColor
/// \~Spanish
/// \brief Ajusta el color del ojo
/// \param eje color del ojo
void  Bird::setEyeColor(QString eye){
    eColor = eye ;
}

/// \fn void  Bird::setFaceColor(QString col)
/// \~English
/// \brief Setter for the face color.
/// \param col face Color
/// \~Spanish
/// \brief Ajusta el color de la cara
/// \param col color de la cara
void  Bird::setFaceColor(QString col){
    color =   col;
}

/// \fn int Bird::getSize()
/// \~English
/// \brief Getter for the bird size.
/// \~Spanish
/// \brief Devuelve el tamano del pajaro.
int Bird::getSize() const {
    return size ;
}

/// \fn int Bird::getEyeBrow()
/// \~English
/// \brief Getter for the bird eyeBrow type
/// \~Spanish
/// \brief Devuelve el tipo de ceja
Bird::EyeBrowType Bird::getEyebrow() const{
    return eyeBrow ;
}

/// \fn void  Bird::setSize(int s)
/// \~English
/// \brief Setter for the bird size.
/// \param s bird size
/// \~Spanish
/// \brief Ajusta el tamano del pajaro
/// \param s tamano del pajaro
void Bird::setSize(int s){

    if(s <= 50)         { size = 50;  }
    else if (s >= 200)  { size = 200; }
    else                { size = s;   }

    resize(size + 2, size + 2) ;
}

/// \fn void  Bird::setEyeBrow(QString eye)
/// \~English
/// \brief Setter for the eyebrow type.
/// \param eb eyebrow type
/// \~Spanish
/// \brief Ajusta el tipo de cejas
/// \param eb tipo de ceja
void Bird::setEyebrow(EyeBrowType eb){
    if(eb < 0 || eb > 3)
        eyeBrow = UNI ;
    else eyeBrow = eb;
}

/// \fn Qt::GlobalColor Bird::getColor(QString color)
/// \~English
/// \brief Converts a color specified as string to a Qt::GlobalColor
/// \param color string with a color
/// \~Spanish
/// \brief Convierte un color especificado como un string a un Qt::GlobalColor
/// \param color cadena de caracteres de un color
Qt::GlobalColor Bird::getColor(QString color) const{

    if(color=="red")
        return Qt::red ;
    else if (color == "green")
        return Qt::green ;
    else if (color == "yellow")
        return Qt::yellow ;
    else if(color == "blue")
        return Qt::blue ;
    else
        return Qt::white ;

}

/// \fn int Bird::randInt(int min, int max)
/// \~English
/// \brief Generates a random integer in the range [min, max]
/// \param min minimum random value
/// \param max maximum random value
/// \~Spanish
/// \brief Genera un entero aleatorio en el rango [min, max]
/// \param min valor aleatorio minimo
/// \param max valor aleatorio maximo
int Bird::randInt(int min, int max){
    return qrand() % ((max +1 )- min) +  min ;
}


/// \fn void Bird::paintEvent(QPaintEvent *event)
/// \~English
/// \brief This function is automatically invoked each time the widget or
/// its parent receives a repaint signal.
/// \~Spanish
/// \brief Esta funcion es invocada automaticamente cada ves que el widget
/// o su padre recive una senal de repintar.
///
void Bird::paintEvent(QPaintEvent *) {
    QPainter p(this);

    QPen outlinePen(Qt::black);
    QPen eyebrowsPen(Qt::black) ;

    int ball_size = getSize() ;
    float half_ball = ball_size/2.0 ;
    float white_ball = ball_size / 4.0 ;
    float black_ball = white_ball * .5 ;
    float up_gap = ball_size * .10 ;
    float eyebrow_gap = ball_size *.18 ;

    outlinePen.setWidth(2);
    eyebrowsPen.setWidth(ball_size *.05);

    // Face
    p.setPen(outlinePen) ;
    p.setBrush(getColor(getFaceColor())) ;
    p.drawEllipse(0, 0, ball_size, ball_size);

    // Eye Balls
    p.setBrush(getColor(getEyeColor())) ;
    p.drawEllipse(half_ball-white_ball,ball_size/2-up_gap,white_ball,white_ball) ; //left eye
    p.drawEllipse(half_ball,ball_size/2-up_gap,white_ball,white_ball) ; //right eye

    // Eye iris
    p.setBrush(Qt::black) ;
    p.drawEllipse(half_ball-black_ball,ball_size/2-up_gap+(white_ball-black_ball)/2.0,black_ball,black_ball) ;
    p.drawEllipse(half_ball,ball_size/2-up_gap+(white_ball-black_ball)/2.0 ,black_ball,black_ball) ;

    // Eyebrow

    p.setPen(eyebrowsPen) ;

    if(eyeBrow == UNI){
        //Normal Eyebrow
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 3*white_ball,ball_size/2-eyebrow_gap) ;
    }
    else if(eyeBrow == ANGRY){

        // Angry Eyebrow
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 2*white_ball, ball_size/2-eyebrow_gap+up_gap) ;
        p.drawLine(2*white_ball, ball_size/2-eyebrow_gap + up_gap, 3*white_ball,ball_size/2-eyebrow_gap) ;
    }
    else if(eyeBrow == UPSET){
        // Angry Eyebrow
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 2*white_ball, ball_size/2-eyebrow_gap+up_gap/2) ;
        p.drawLine(2*white_ball, ball_size/2-eyebrow_gap + up_gap/2, 3*white_ball,ball_size/2-eyebrow_gap) ;

    }
    else{
        // Hairy
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 2*white_ball, ball_size/2-eyebrow_gap+up_gap) ;
        p.drawLine(2*white_ball, ball_size/2-eyebrow_gap + up_gap, 3*white_ball,ball_size/2-eyebrow_gap) ;
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 2*white_ball, ball_size/2-eyebrow_gap+up_gap/2) ;
        p.drawLine(2*white_ball, ball_size/2-eyebrow_gap + up_gap/2, 3*white_ball,ball_size/2-eyebrow_gap) ;

    }

}
