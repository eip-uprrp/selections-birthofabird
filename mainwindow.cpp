// RAN [2014-06-03]
//   - Set initial size of the window to 1000,1000. Adjust the
//     mainView and scene accordingly.

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTime>
#include <QTimer>
#include <QDebug>
#include <ctime>

/// \fn MainWindow::MainWindow(QWidget *parent)
/// \~English
/// \brief Constructor
/// \~Spanish
/// \brief Constructor
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    resize(1000,1000);
    scene = new QGraphicsScene(this) ;
    ui->mainView->resize(width()*.95,height()*.95);
    scene->setSceneRect(QRectF(QPoint(0,0), QPoint(width()*.95,height()*.95))) ;
    ui->mainView->setScene(scene) ;
    ui->mainView->setAlignment((Qt::AlignLeft | Qt::AlignTop));
    QTime now = QTime::currentTime();
    qsrand(now.msec());
    srand(time(NULL)) ;


}

/// \fn MainWindow::~MainWindow(QWidget *parent)
/// \~English
/// \brief Destructor
/// \~Spanish
/// \brief Destructor
MainWindow::~MainWindow()
{
    delete ui;
    delete scene;
}

/// \fn void MainWindow::addBird(int x, int y, Bird &b)
/// \~English
/// \brief Paints a bird into the MainWindow
/// \param x X coordinate of the Main Window
/// \param y Y coordinate of the Main Window
/// \param b Bird object to be painted
/// \~Spanish
/// \brief Pinta un pajaro en la pantalla principal
/// \param x Coordenada X de la pantalla principal
/// \param y Coordenada Y de la pantalla principal
/// \param b Objecto Bird a ser pintado
void MainWindow::addBird(int x, int y, Bird &b){
    scene->addWidget(&b) ;
    b.move(x,y) ;
}

/// \fn void MainWindow::addBird(Bird &b)
/// \~English
/// \brief Paints a bird into the MainWindow
/// \param b Bird object to be painted
/// \~Spanish
/// \brief Pinta un pajaro en la pantalla principal
/// \param b Objecto Bird a ser pintado
void MainWindow::addBird(Bird &b){
    scene->addWidget(&b) ;
}
